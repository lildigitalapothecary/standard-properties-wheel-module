var CircleGenerator = (function() {

	var cENS = document.createElementNS.bind(document),
		cE = document.createElement.bind(document);

	var _setAttributes = function(el, attrs, attrType) {
		for ( var key in attrs ) {
			( attrType === 'setAttributeNS' ) ? el.setAttributeNS(null, key, attrs[key]) 
													: 
												el.setAttribute(key, attrs[key]);
		}
	}

	var _renderMultipleNodes = function(parent, children) {
		return children.map(function(child) {
			parent.appendChild(child);
			return child;
		});
	}

	var _renderSingleNode = function(parent, child) {
		parent.appendChild(child);
		return child;
	}

	var _generateContainerEl = function(containerEl) {
		var container = cE(containerEl);
		return function(containerData) {
			_setAttributes(container, containerData);
			return container;
		}
	}

	var _generateMultipleEls = function(cb, data) { return data.map(cb) }

	var _generateTextEl = function(textDatum) {		
		var textEl = cE('h3');
		textEl.innerHTML = textDatum;
		return textEl;		
	}

	var _generateIcon = function(iconDatum) {		
		var icon = cE('img');
		_setAttributes(icon, { src: iconDatum });
		return icon;
	}

	var _generateSector = function(sectorDatum) {
		var sector = cENS( "http://www.w3.org/2000/svg","path" );
		_setAttributes(sector, {
			fill: sectorDatum.color,
			d: 'M' + sectorDatum.L + ',' + sectorDatum.L + ' L' + sectorDatum.L + ',0 A' + sectorDatum.L + ',' + sectorDatum.L + ' 1 0,1 ' + sectorDatum.X + ', ' + sectorDatum.Y + ' z',
			transform: 'rotate(' + sectorDatum.R + ', '+ sectorDatum.L+', '+ sectorDatum.L+')',
			'stroke-width': 7.5,
			stroke: '#fff',
			'page-id': 'home',
			'post-id': sectorDatum.url 
		}, 'setAttributeNS');						
	    return sector;
	}

	var _generateCircle = function(circleData) {
		var circle = cENS( "http://www.w3.org/2000/svg","circle" );
		_setAttributes(circle, {
			cx: circleData.circleSize * 0.5,
			cy: circleData.circleSize * 0.5,
			r: circleData.circleSize * 0.24,
			fill: circleData.circleColor
		}, 'setAttributeNS');
		return circle;
	}

	var _generateSVG = function(svgData) {
		var svg = cENS( "http://www.w3.org/2000/svg","svg" );
		_setAttributes(svg, { style: "width: "+ svgData.svgSize +"px; height: " + svgData.svgSize + "px" });
		return svg;
	}

	var _renderDOM = function(parent, childrenData, cb) {			    
	    return ( Array.isArray(childrenData) ) ? _renderMultipleNodes(parent, cb(childrenData)) 
	    											: 
	    										 _renderSingleNode(parent, cb(childrenData));
	}

	var _bindEvents = function(svg) {
		svg.addEventListener('click', function(e) {
			if ( e.target.tagName === 'path' ) {
				console.log('a path was clicked, woopdie doo');
				// that.pushPageNav(e.target.getAttribute('page-id'), e.target.getAttribute('post-id'));
			}
		});
	}	

	var _setCircleSectorData = function(data) {
		var color = data.circleSectorColor,
			percentage = 1 / data.sectors.length,
	    	l = data.size / 2;

	    return data.sectors.map(function(item, i) {
	        var a = 360 * percentage,
	        	aCalc = ( a > 180 ) ? 360 - a : a,
		        aRad = aCalc * Math.PI / 180,
		        z = Math.sqrt( 2*l*l - ( 2*l*l*Math.cos(aRad) ) ),
		        x = ( aCalc <= 90 ) ? l*Math.sin(aRad) : l*Math.sin((180 - aCalc) * Math.PI/180 ),
	        	y = Math.sqrt( z*z - x*x ),
	        	r = (a * i) + data.sectorRotationOffset;        

	        if( a <= 180 ) {
	            x = l + x;
	            arcSweep = 0;
	        }
	        else {
	            x = l - x;
	            arcSweep = 1;
	        }

	        return {
	        	percentage: percentage,
	            label: item.the_title,
	            arcSweep: arcSweep,
	            color: color,
	            url: item.component_url, 
	            L: l,
	            X: x,
	            Y: y,
	            R: r
	        }
	    });
	}

	var _getArrObjVals = function(arr, prop) {
		return arr.map(function(arrEl) {
			return arrEl[prop];
		});
	}

	var init = function(data) {
	var qS = document.querySelector.bind(document),
		circleContainer = qS('.circle_container'),
		generateIconContainer = _generateContainerEl('div'),
		generateTextContainer = _generateContainerEl('div');

	var sectorData = _setCircleSectorData(data),
		iconContainer = _renderDOM(circleContainer, { class: 'icon_container', style: 'height:' + data.size * .475 + 'px;width:' + data.size * .475 + 'px;' }, generateIconContainer),
		icons = _renderDOM(iconContainer, _getArrObjVals(data.sectors, 'featuredImage'), _generateMultipleEls.bind(null, _generateIcon)),
		textContainer = _renderDOM(circleContainer, { class: 'text_container', style: 'height:' + data.size * .775 + 'px;width:' + data.size * .775 + 'px;' }, generateTextContainer),
		textEls = _renderDOM(textContainer, _getArrObjVals(data.sectors, 'the_title'), _generateMultipleEls.bind(null, _generateTextEl)),
		svg = _renderDOM(circleContainer, { svgSize : data.size }, _generateSVG),
		paths = _renderDOM(svg, sectorData, _generateMultipleEls.bind(null, _generateSector)),
		circle = _renderDOM(svg, { circleSize: data.size, circleColor: data.circleColor }, _generateCircle);

		_bindEvents(svg);
	}

	var publicAPI = {
		init: init	
	}

	return publicAPI;

})();

var data = {
	size: 650,
	circleColor: '#fff',
	circleSectorColor: 'rgb(28, 58, 101)',
	sectorRotationOffset: 45,
	sectors: [
		{ the_title: 'Component 1', featuredImage: './images/CircleBkgrd_Cloud.png', component_url: 'google.com' },
		{ the_title: 'Component 2', featuredImage: './images/CircleBkgrd_DollarSign.png', component_url: 'facebook.com' },
		{ the_title: 'Component 3', featuredImage: './images/CircleBkgrd_Handshake.png', component_url: 'twitter.com' },
		{ the_title: 'Component 4', featuredImage: './images/CircleBkgrd_World.png', component_url: 'instagram.com' }
	]
};

CircleGenerator.init(data);